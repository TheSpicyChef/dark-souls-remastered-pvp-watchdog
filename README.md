Dark Souls Remastered PVP Watchdog
=======================

## About

This is a port of the original Dark Souls PvP Watchdog made by /u/eur0pa. Although this may not catch everything, it should mitigate 90% of what cheaters can do.

## What it does

This tool identifies cheaters and attempts to mitigate the damage which they can do through multiple methods.

 * It will detect anomalies and blatant forms of cheating, allowing you to remove the offender: soul level and stats coherence; amount of hp related to current equip and/or stats; spells (restricted spells, covenant-only spells, amount of casts) and equipment (un-obtainable, restricted or equipped in the wrong slots).
 * It will prevent dangerous effects such as curse and egghead from being applied to you during pvp
 * It will prevent people from becoming invisible (they will show up as 'hacked equip' and DSPW will let you kick them out) � it won't prevent the invisibility bug due to de-synchronization and lag from happening though
 * It will play a sound when invading or getting invaded, useful if the game is out of focus
 * If enabled, it can autokick detected cheaters when hosting
 * If enabled, it can backup saves every time a new session is established

## Will this softban me

Although nothing is 100% safe, this tool shouldn't softban as I've avoided using methods which could potentially ban. 
If you're worried, don't use it but i've been debugging it for a while and am still not banned.

## How to use it

 1. Get the package from the download page
 2. Extract everything into your Dark Souls Remastered folder, generally "C:\Program Files (x86)\Steam\steamapps\common\DARK SOULS REMASTERED"
 3. Edit DSRPWSettings.ini to your likings - lines starting with ; describe what each setting does
 4. Play Dark Souls Remastered unfettered by cheaters

#### Example in-game overlay output

    (cheater, red color)
     !1 player1 [F1] to kick    [F5] to ignore   SL [Real 110-115]
     ^^ ^       ^               ^                ^ anomaly detected and details (if available)
     || |       +---------------+ commands (if available)
     || |         
     || | 
     || + player name
     |+ player id
     + cheating detected

    (ignored player or player that's being blocked and is currently leaving the session, green color)
     @2 player2 
     ^ ignored / whitelisted / leaving

    (common player, white)
     #3 player3 
     ^ no anomalies detected

## known issues

Post any bugs or issues to the issue tracker: [here](https://bitbucket.org/TheSpicyChef/dark-souls-remastered-pvp-watchdog/issues)
If Watchdog doesn't protect you from something, post it to the issue tracker.

## appendix

### keyboard shorctuts reference

[**F1**] ..... banish all detected cheaters
[**F5**] ..... ignore all detected cheaters
[**F9**] ..... toggle the in-game overlay
[**F10**] .... show the about window
[**F11**] .... Allow ignored cheaters to be kicked again

### changelog and latest fixes

** see CHANGELOG.txt **

### package contents

d3d11.dll ....................... the dark souls pvp watchdog dll

DSRPWSettings.ini .................. the options file

changelog.txt .................. change history for the tool

readme.txt ..................... the text document you're currently looking at


### resources

Original Dark Souls Watchdog: https://bitbucket.org/infausto/dark-souls-pvp-watchdog/

### thanks to

[Eur0pa](http://reddit.com/u/eur0pa) for the original code i've ported to Dark Souls Remastered. Thank him for the original concept and tool. Buy him a beer or something idk.
